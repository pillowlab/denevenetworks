function varargout = GLM_GUI(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GLM_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GLM_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function GLM_GUI_OpeningFcn(hObject, eventdata, handles, varargin)

handles.tau = 20;
handles.alpha = 0;
handles.tauc = 10; %Decay of stimulu

handles.NT =50; %Max time
handles.dt = 0.1; %dt, arbitrary units
handles.time = 0:handles.dt:handles.NT; %Time vector
handles.T = 0.2; %Thress
handles.beta = 0;
handles.d = 0;

 
handles.reps = 1000; %Repetitions of the GLM neuron with the same stimulus

%Create stimulus
axes(handles.Stim)
handles.c = newc(handles.NT, handles.tauc, handles.dt);

%Run code
[handles.vnr, handles.dv, handles.o] = execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH); 
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

function [stim] = newc(NT, tauc, dt)
time = 0:NT;
nt = length(time);
stim = zeros(1,nt);
lambda_c = 1/(2*tauc); %Decay

for i = 2:nt
    dc = -lambda_c*stim(i-1) + 0.1*randn();
    stim(i) = stim(i-1) + dc;
end

%Interpolates plot to account for chosen dt, so that each dt gets the same
%'shape' of plot.Meant to address the problem of very jagged/noisy stimulus
%as dt got smaller. 
xq = 0:dt:NT;
stim = interp1(time, stim, xq, 'linear');

%Plot stimulus
handles.h1 = plot(xq, stim);

return

function [lif_vnr, lif_dv, lif_sp] = execute(reps, time, tau, T, dt, c, alpha, beta, d, rasteraxis, psthaxis)
nt = length(time);
rasterplot = zeros(reps+1, nt);
[lif_sp, ~, lif_vnr, lif_dv] = lifsim(time, tau, T, dt, c);
rasterplot(reps+1,:) = lif_sp;
for i = reps:-1:1
    [glm_sp, ~] = glmsim(time, tau, T, dt,c, alpha, beta, d);
    rasterplot(i, :) = glm_sp;
end
[y,x] = find(rasterplot(1:reps,:));
[y1,x1] = find(rasterplot(reps+1,:));
axes(rasteraxis)
scatter(x,y, '.')
hold on
scatter(x1,y1+reps,'rx')
hold off
axis([0 nt 0 reps+1])
mean = sum(rasterplot(1:reps,:),1)./reps;

axes(psthaxis)
plot(mean)
hold on
scatter(x1, y1-0.5, 'rx')
hold off
axis([0 nt 0 0.8])
    

return

function [lif_sp, lif_v, lif_vnr, dv] = lifsim(time, tau, T, dt, c) 
nt = length(time);
lambda = 1/(tau); %Decay

lif_v = zeros(1,nt); %Voltages
lif_sp = zeros(1,nt); %Spike times
lif_vnr = zeros(1,nt);
dv = zeros(1, nt);

for i = 2:nt
    
    dv(i) = -lambda*lif_v(i-1) + c(i);
    lif_v(i) = lif_v(i-1) + dv(i)*dt;
    lif_vnr(i) = lif_v(i);
    if lif_v(i) > T
        lif_sp(i) = 1; %Spike event
        lif_v(i) = -2*T; %Reset to -2*Threshold 
    end
end

return

function [glm_sp, glm_v] = glmsim(time, tau, T, dt, c, alpha, beta, d)
nt = length(time);
%Initializing voltage and spike time arrays
glm_v = zeros(1,nt);
glm_sp = zeros(1,nt);
%Decay
lambda = 1/tau;
rt = zeros(1,nt);
Ps = zeros(1,nt);

for i = 2:nt
    if glm_sp(i-1) == 1
        dv = 0;
    else
        dv = -lambda*glm_v(i-1) + c(i);
    end
    glm_v(i) = glm_v(i-1) + dv*dt;
    rt(i) = exp(alpha*(glm_v(i)-T) + beta*dv + d); %Rate
    Ps(i) = (1-exp(-rt(i)*dt)); 
    
    if rand() < Ps(i) %Condition for spiking
        glm_sp(i) = 1; %Spike
        glm_v(i) = -2*T; %Reset
    end

end


return

function varargout = GLM_GUI_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%Push button to create new stimulus and plot LIF and GLM responses
function pushbutton1_Callback(hObject, eventdata, handles)
axes(handles.Stim)
handles.c = newc(handles.NT, handles.tau, handles.dt); %New stimulus
% handles.alpha = get(handles.slider1, 'Value');
% handles.beta = get(handles.slider2, 'Value');
set(findobj('Tag','text4'),'String', strcat('Alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text5'),'String', strcat('Beta = ', num2str(handles.beta)))
set(findobj('Tag','text6'),'String', strcat('d = ', num2str(handles.d)))
[handles.vnr, handles.dv, handles.o]= execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH); 

guidata(hObject, handles)

%Slider to adjust alpha value -> rate = exp(-alpha(V - Thresh))
function slider1_Callback(hObject, eventdata, handles)
handles.alpha = get(hObject, 'Value');
handles.beta = get(handles.slider2, 'Value');
set(findobj('Tag','text2'),'String', strcat('alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text4'),'String', strcat('Alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text5'),'String', strcat('Beta = ', num2str(handles.beta)))
set(findobj('Tag','text6'),'String', strcat('d = ', num2str(handles.d)))
%Replot with new alpha
%No new stimulus created
[handles.vnr, handles.dv, handles.o]= execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH); 
guidata(hObject, handles)

function slider1_CreateFcn(hObject, eventdata, handles)
set(hObject, 'Max', 10000, 'Min', 0) %Max and min alpha values
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%Changes value of dt, creates new stimulus and response 
%NOTE: Values less than 0.00001 are very slow (on my computer, at least)
function edit1_Callback(hObject, eventdata, handles)
%Get values of dt and make a new time array
newdt = str2double(get(hObject, 'String'));
if (handles.dt ~= newdt)
    handles.dt = newdt;
    newtime = 0:handles.dt:handles.NT;
    %Interpolate old stim for new time vect
    handles.c = interp1(handles.time, handles.c, newtime);
    handles.time = newtime;
end
handles.alpha = get(handles.slider1, 'Value');
handles.beta = get(handles.slider2, 'Value');
set(findobj('Tag','text4'),'String', strcat('Alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text5'),'String', strcat('Beta = ', num2str(handles.beta)))
set(findobj('Tag','text6'),'String', strcat('d = ', num2str(handles.d)))
%New GLM and LIF response
[handles.vnr, handles.dv, handles.o] = execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH); 
guidata(hObject, handles)
function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)
handles.reps = str2double(get(hObject, 'String'));
handles.alpha = get(handles.slider1, 'Value');
handles.beta = get(handles.slider2, 'Value');
set(findobj('Tag','text4'),'String', strcat('Alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text5'),'String', strcat('Beta = ', num2str(handles.beta)))
set(findobj('Tag','text6'),'String', strcat('d = ', num2str(handles.d)))
%Plot GLM, LIF with same stimulus
[handles.vnr, handles.dv, handles.o] = execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH); 
guidata(hObject, handles)
function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
handles.beta = get(hObject, 'Value');
handles.alpha = get(handles.slider1, 'Value');
set(findobj('Tag','text3'),'String', strcat('beta = ', num2str(handles.beta)))
set(findobj('Tag','text4'),'String', strcat('Alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text5'),'String', strcat('Beta = ', num2str(handles.beta)))
set(findobj('Tag','text6'),'String', strcat('d = ', num2str(handles.d)))
%Replot with new beta
%No new stimulus created
[handles.vnr, handles.dv, handles.o] = execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH);
guidata(hObject, handles)

function slider2_CreateFcn(hObject, eventdata, handles)
set(hObject, 'Max', 50, 'Min', -50) %Max and min alpha values
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit3_Callback(hObject, eventdata, handles)
handles.beta = str2double(get(hObject, 'String'));
handles.alpha = get(handles.slider1, 'Value');
%Plot GLM, LIF with same stimulus
set(findobj('Tag','text4'),'String', strcat('Alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text5'),'String', strcat('Beta = ', num2str(handles.beta)))
set(findobj('Tag','text6'),'String', strcat('d = ', num2str(handles.d)))
[handles.vnr, handles.dv, handles.o] = execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH); 
guidata(hObject, handles)
% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
options = optimset('Display', 'on','largescale', 'off');
fun = @(x) Loss(x, handles.vnr, handles.dv, handles.o, handles.T);
x0 = [0;0;0];
[y,~] = fminunc(fun,x0, options);
handles.alpha = y(1);
handles.beta = y(2);
handles.d = y(3);

disp(y)
guidata(hObject, handles)




% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
[handles.vnr, handles.dv, handles.o] = execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH); 
set(findobj('Tag','text4'),'String', strcat('Alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text5'),'String', strcat('Beta = ', num2str(handles.beta)))
set(findobj('Tag','text6'),'String', strcat('d = ', num2str(handles.d)))
guidata(hObject, handles)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
handles.d = 0;
[handles.vnr, handles.dv, handles.o] = execute(handles.reps, handles.time, handles.tau, handles.T, handles.dt, handles.c, ...
    handles.alpha, handles.beta, handles.d, handles.Raster, handles.PSTH);
set(findobj('Tag','text4'),'String', strcat('Alpha = ', num2str(handles.alpha)))
set(findobj('Tag','text5'),'String', strcat('Beta = ', num2str(handles.beta)))
set(findobj('Tag','text6'),'String', strcat('d = ', num2str(handles.d)))
guidata(hObject, handles)
