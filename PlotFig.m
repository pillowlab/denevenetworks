figure(2)

re = 1/255*[255,51,51];
bl = 1/255*[0,128,255];
gr = 1/225*[22, 183, 32];


subplot(2,2,1)
[iiinh,jjinh] = find(ot(1:ncells/2,:));
[iiexc,jjexc] = find(ot(ncells/2+1:ncells,:));
plot(jjinh*dt,iiinh,'.', 'Color', re)
hold on
plot(jjexc*dt,iiexc+ncells/2,'.','Color', bl);
 ylabel('Neuron'); xlabel('Time')
set(gca, 'ylim',[0 ncells])
box off

subplot(2,2,2)
[iiinh,jjinh] = find(o(1:ncells/2,:));
[iiexc,jjexc] = find(o(ncells/2+1:ncells,:));
plot(jjinh*dt,iiinh,'.', 'Color', re)
hold on
plot(jjexc*dt,iiexc+ncells/2,'.','Color', bl);
 ylabel('Neuron')
set(gca, 'ylim',[0 ncells])
box off

subplot(2,2,3)

plot(t,xt0, 'Color',re)
hold on
h = plot(t,xhat,'--', 'Color', bl); set(h, 'linewidth', 2);
%legend('Target', 'Estimate');
ylim([-10 20])
xlabel('Time')
box off

subplot(2,2,4)
plot(t,xt0_1, 'Color',re)
hold on
h = plot(t,xhat_1,'--', 'Color', bl); set(h, 'linewidth', 2);
xlabel('Time')
box off

%%
figure(3)

[iiinh,jjinh] = find(ot(1:ncells/2,:));
[iiexc,jjexc] = find(ot(ncells/2+1:ncells,:));
plot(jjinh*dt,iiinh,'.', 'Color', re)
hold on
plot(jjexc*dt,iiexc+ncells/2,'.','Color', bl);
 ylabel('Neuron'); xlabel('Time')
set(gca, 'ylim',[0 ncells])
box off

figure(4)

[iiinh,jjinh] = find(o(1:ncells/2,:));
[iiexc,jjexc] = find(o(ncells/2+1:ncells,:));
plot(jjinh*dt,iiinh,'.', 'Color', re)
hold on
plot(jjexc*dt,iiexc+ncells/2,'.','Color', bl);
 ylabel('Neuron'); xlabel('Time')
set(gca, 'ylim',[0 ncells])
box off

%%
alph = .01;
for j = 1:7
xval = -20:0.1:20;
alph = alph*4;
y = zeros(1, length(xval));
for i = 1:length(xval)
    y(i) = 1/(1+exp(-alph*(xval(i))));
end
hold on
plot(xval, y)
end
grid on
box off
ylabel('Probability of spiking')
set(gca, 'FontSize', 12)

%%


