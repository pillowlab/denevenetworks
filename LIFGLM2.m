NT =50; %Max time
dt = .1; %dt, arbitrary units
time = dt:dt:NT; %Time vector
T = 0.2; % threshold
nt = length(time); % number of time bins

tau = 20;
alpha = 100;
tauc = 10; %Decay of stimulus
beta = .3;

reps = 20; % number of repeats of GLM simulation
rasterplot = zeros(reps+1, nt);

c = zeros(1,nt);
lambda_c = 1/(2*tauc); %Decay

% --- Make OU stimulus -----
for i = 2:nt
    dc = -lambda_c*c(i-1) + 0.1*randn();
    c(i) = c(i-1) + dc;
end

% % --- Make smoothed Gaussian stimulus----
% c = gsmooth(randn(nt,1),10);
% lambda = 1/(tau); %Decay


lif_v = zeros(1,nt); %Voltages
lif_sp = zeros(1,nt); %Spike times
lif_vnr = zeros(1,nt);
dv = zeros(1,nt);

subplot(221);
plot(c);
title('stimulus'); axis tight;
xlabel('time (bins)');


%% Simulate LIF neuron
for i = 2:nt
    if lif_sp(i-1)>0
        dv(i) = 0;
    else
        dv(i) = -lambda*lif_v(i-1) + c(i);
    end
    lif_v(i) = lif_v(i-1) + dv(i)*dt;
    lif_vnr(i) = lif_v(i);
    if lif_v(i) > T
        lif_sp(i) = 1; %Spike event
        lif_v(i) = -2*T; %Reset to -2*Threshold
    end
end
tsp = dt*find(lif_sp); % spike times
rasterplot(reps+1,:) = lif_sp;

subplot(222);
plot(time,lif_vnr, time, dv, 'k',tsp, T, 'rx');

title('V');





%% Start GLM

alpha = 100;
Mtsp = [];  % spike times
for j = reps:-1:1
    %Initializing voltage and spike time arrays
    glm_v = zeros(1,nt);
    glm_sp = zeros(1,nt);
    
    %Decay
    rt = zeros(1,nt);
    Ps = zeros(1,nt);
    
    for i = 2:nt
        if glm_sp(i-1)>0
            gldv = 0;
        else
            gldv = -lambda*glm_v(i-1) + c(i);
        end
        glm_v(i) = glm_v(i-1) + gldv*dt;
        rt(i) = exp(alpha*(glm_v(i)-T) + beta*gldv); %Rate
        Ps(i) = (1-exp(-rt(i)*dt));
        
        if rand() < Ps(i) %Condition for spiking
            glm_sp(i) = 1; %Spike
            glm_v(i) = -2*T; %Reset
        end
        
    end
    Mtsp{j} = find(glm_sp)*dt;
    rasterplot(j, :) = glm_sp;
end
tspglm = find(glm_sp)*dt;
subplot(223);
cla;
ntoplot = reps;
plotRaster(Mtsp);
set(gca,'xlim', [0 nt*dt
    ]);
title('GLM repeats');
hold off;
%% Now do GLM fitting for all params

% Fit GLM
options = optimset('Display', 'iter', 'largescale', 'off');
fun = @(x) Loss_bernoulliGLM(x, lif_vnr, dv, lif_sp, T);
x0 = [0;0;0];
[theta] = fminunc(fun,x0, options);

theta

% Extract ML param estimates
alpha_hat = theta(1);
beta_hat = theta(2);
d_hat = theta(3);

subplot(426);
lambda_pred = (alpha_hat*(lif_vnr-T)+beta_hat*dv+d_hat);
plot(time, lambda_pred, tsp, 1, 'rx');



%% Simulate GLM with fitted params

for j = 1:20
    n_glm_v = zeros(1,nt);
    n_glm_sp = zeros(1,nt);
    n_glm_vnr = zeros(1,nt);
    lam = zeros(1,nt);
    for i = 2:nt
        
        if n_glm_sp(i-1)>0
            gldv = 0;
        else
            gldv = -lambda*glm_v(i-1) + c(i);
        end
        
        gldv = -lambda*n_glm_v(i-1) + c(i);
        n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
        n_glm_vnr(i) = n_glm_v(i);
        rt(i) = exp(alpha_hat*(n_glm_v(i)-T)+ beta_hat*gldv+d_hat); %Rate
        Ps(i) = (1-exp(-rt(i)*dt));
        lam(i) = alpha_hat*(n_glm_v(i)-T) + beta_hat*gldv+d_hat;
        if rand() < Ps(i) %Condition for spiking
            n_glm_sp(i) = 1; %Spike
            n_glm_v(i) = -2*T; %Reset
        end
        
    end
    Mtsp{j} = find(n_glm_sp)*dt;
end
tspnglm =find(n_glm_sp)*dt;

subplot(223);
plotRaster(Mtsp);
set(gca,'xlim', [0 nt*dt
    ]);
title('GLM repeats');


subplot(428);
logrtplot = log(max(rt,1e-200));
plot(time, logrtplot, '.-', find(n_glm_sp)*dt, T, 'rx');
plot(time, lam)
hold on
plot(time, lambda_pred)
hold off

%% Make figs

re = 1/255*[255,51,51];
bl = 1/255*[0,128,255];
gr = 1/225*[22, 183, 32];
figure(2)
plot(time, c, 'Color', bl)
title 'Stimulus'
xlabel 'Time'
grid on
grid off
box off
set(gca, 'FontSize', 12)
saveas(gca, 'OUStim', 'png')

figure(3)
clf
plot(time, lif_vnr, 'Color', re)
hold on
plot(time, n_glm_vnr, 'Color', bl)
legend('LIF neuron', 'GLM neuron')
hold on
plot(tsp, T,'x', 'Color', re)
hold on
plot(tspnglm, T, 'x', 'Color', bl)
hold off
title 'Voltage'
xlabel 'Time'
box off
set(gca, 'FontSize', 12)
saveas(gca, 'SingleNeuronFit', 'png')

%% =================================
% Now fix alpha and just fit beta and d

alpha_fix = 100;
fun2 = @(prs)Loss_bernoulliGLM([alpha_fix; prs], lif_vnr, dv, lif_sp, T);

prs0 = [0;0];

theta2 = fminunc(fun2,prs0, options);

theta2
beta_hat2 = theta2(1);
d_hat2 = theta2(2);

%% Simulate GLM with fitted params

for j = 1:20
    n_glm_v = zeros(1,nt);
    n_glm_sp = zeros(1,nt);
    lam2 = zeros(1,nt);
    for i = 2:nt
        
        if n_glm_sp(i-1)>0
            gldv = 0;
        else
            gldv = -lambda*glm_v(i-1) + c(i);
        end
        gldv = -lambda*n_glm_v(i-1) + c(i);
        n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
        lam2(i) = alpha_fix*(n_glm_v(i)-T) + beta_hat2*gldv+d_hat2;
        rt(i) = exp(alpha_fix*(n_glm_v(i)-T) + beta_hat2*gldv+d_hat2); %Rate
        Ps(i) = (1-exp(-rt(i)*dt));
        
        if rand() < Ps(i) %Condition for spiking
            n_glm_sp(i) = 1; %Spike
            n_glm_v(i) = -2*T; %Reset
        end
        Mtsp{j} = find(n_glm_sp)*dt;
    end
end

subplot(223);
plotRaster(Mtsp);
set(gca,'xlim', [0 nt*dt
    ]);
title('GLM repeats');
hold off;
% logrtplot = log(max(rt,1e-200));
% plot(time, logrtplot, '-', find(n_glm_sp)*dt, T, 'rx');
% plot(time, lam2*200)
% hold on
% plot(time, lam)
% hold on
% plot(time, lambda_pred)
% hold off
%% ======================
% No d GLM fitting

% Fit GLM
options = optimset('Display', 'iter', 'largescale', 'off');
fun = @(x) Loss_bernoulliGLMnd(x, lif_vnr, dv, lif_sp, T);
x0 = [0;0];
[theta] = fminunc(fun,x0, options);

theta

% Extract ML param estimates
alpha_hat = theta(1);
beta_hat = theta(2);

subplot(426);
%lambda_pred = (alpha_hat*(lif_vnr-T)+beta_hat*dv);
plot(time, lambda_pred, tsp, 1, 'rx');



%% Simulate GLM with fitted params

for j = 1:20
    n_glm_v = zeros(1,nt);
    n_glm_sp = zeros(1,nt);
    lam3 = zeros(1,nt);
    for i = 2:nt
        
        if n_glm_sp(i-1)>0
            gldv = 0;
        else
            gldv = -lambda*glm_v(i-1) + c(i);
        end
        
        gldv = -lambda*n_glm_v(i-1) + c(i);
        n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
        rt(i) = exp(alpha_hat*(n_glm_v(i)-T)+ beta_hat*gldv); %Rate
        Ps(i) = (1-exp(-rt(i)*dt));
        lam3(i) = alpha_hat*(n_glm_v(i)-T) + beta_hat*gldv;
        if rand() < Ps(i) %Condition for spiking
            n_glm_sp(i) = 1; %Spike
            n_glm_v(i) = -2*T; %Reset
        end
        
    end
    Mtsp{j} = find(n_glm_sp)*dt;
end

subplot(428);
%logrtplot = log(max(rt,1e-200));
%plot(time, logrtplot, '.-', find(n_glm_sp)*dt, T, 'rx');
plot(time, lam)
hold on
plot(time, lambda_pred)
hold on
plot(time, lam3)
hold off
