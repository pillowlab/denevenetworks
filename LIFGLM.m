NT =50; %Max time
dt = 0.1; %dt, arbitrary units
time = 0:dt:NT; %Time vector
T = 0.2;
nt = length(time);

tau = 20;
alpha = 1000;
tauc = 10; %Decay of stimulus
beta = .3;

reps = 1000;
rasterplot = zeros(reps+1, nt);

c = zeros(1,nt);
lambda_c = 1/(2*tauc); %Decay


for i = 2:nt
    dc = -lambda_c*c(i-1) + 0.1*randn();
    c(i) = c(i-1) + dc;
end

lambda = 1/(tau); %Decay

lif_v = zeros(1,nt); %Voltages
lif_sp = zeros(1,nt); %Spike times
lif_vnr = zeros(1,nt);
dv = zeros(1,nt);

for i = 2:nt
    
    dv(i) = -lambda*lif_v(i-1) + c(i);
    lif_v(i) = lif_v(i-1) + dv(i)*dt;
    lif_vnr(i) = lif_v(i);
    if lif_v(i) > T
        lif_sp(i) = 1; %Spike event
        lif_v(i) = -2*T; %Reset to -2*Threshold 
    end
end

rasterplot(reps+1,:) = lif_sp;

%Initializing voltage and spike time arrays
glm_v = zeros(1,nt);
glm_sp = zeros(1,nt);
%Decay
rt = zeros(1,nt);
Ps = zeros(1,nt);

for j = reps:-1:1
    
for i = 2:nt
    gldv = -lambda*glm_v(i-1) + c(i);
    glm_v(i) = glm_v(i-1) + gldv*dt;
    rt(i) = exp(alpha*(glm_v(i)-T) + beta*gldv); %Rate
    Ps(i) = (1-exp(-rt(i)*dt)); 
    
    if rand() < Ps(i) %Condition for spiking
        glm_sp(i) = 1; %Spike
        glm_v(i) = -2*T; %Reset
    end

end
rasterplot(j, :) = glm_sp;
end
subplot(2,2,1)
plot(time, c)

subplot(2,2,2)
[y,x] = find(rasterplot(1:reps,:));
[y1,x1] = find(rasterplot(reps+1,:));
scatter(x,y, '.')
hold on
scatter(x1,y1+reps,'rx')
hold off
axis([0 nt 0 reps+1])
mean = sum(rasterplot(1:reps,:),1)./reps;

subplot(2,2,3)
plot(mean)
hold on
scatter(x1, y1, 'rx')
hold off
axis([0 nt 0 1.1])

options = optimoptions(@fminunc, 'Display', 'none');
fun = @(x) Loss(x, lif_vnr, dv, lif_sp, T);
x0 = [0;0;0];
[y,~] = fminunc(fun,x0, options);
alpha = y(1);
beta = y(2);
d = y(3);

n_glm_v = zeros(1,nt);
n_glm_sp = zeros(1,nt);

for i = 2:nt
    gldv = -lambda*n_glm_v(i-1) + c(i);
    n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
    rt(i) = exp(alpha*(n_glm_v(i)-T) + beta*gldv); %Rate
    Ps(i) = (1-exp(-rt(i)*dt)); 
    
    if rand() < Ps(i) %Condition for spiking
        n_glm_sp(i) = 1; %Spike
        n_glm_v(i) = -2*T; %Reset
    end

end

subplot(2,2,4)
plot(n_glm_sp, '.')
hold on
plot(lif_sp,'rx')
hold off
