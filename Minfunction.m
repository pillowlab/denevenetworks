%cd ../../Desktop/Thesis/Code
%%
%load Analyze.mat
%%
options = optimset('Display', 'off','Algorithm','interior-point');
%d = noise_1;
vval = cell(7, 400);
vnrval = cell(7, 400);
sl = zeros(1, 7);
for i = 1:1
    tic
    %d = data{i};
    silence = 0;
    for j = 1:400
        aa = find(d.o(j,:) == 1);
        if ~isempty(aa)
            fun = @(x) Loss_bernoulliGLM([100, x], d.v(j,:), d.dv(j,:), d.o(j,:), d.T(j));
            x0 = [0,0];
            [y,fval] = fminunc(fun,x0, options);
            vval{i,j} = [y,fval];
            
            fun = @(x) Loss_bernoulliGLM([100, x], d.vnr(j,:), d.dv(j,:), d.o(j,:), d.T(j));
            x0 = [0,0];
            [y,fval] = fminunc(fun,x0, options);
            vnrval{i,j} = [y,fval];
        else
            silence = silence + 1;
        end
        
    end
    sl(i) = silence;
    toc
    
end

%%
N = 400;
alpha_est = zeros(2,N);
beta_est = zeros(2, N);
b = zeros(2,N);
objval = zeros(2,N);
vvnr = vnrval(1,:);
vv = vval(1,:);

for i = 1:N
    if ~isempty(vvnr{i})
        alpha_est(:,i) = [vvnr{i}(1); vv{i}(1)];
        beta_est(:,i) = [vvnr{i}(2); vv{i}(2)];
        b(:,i) = [vvnr{i}(3); vv{i}(3)];
        %objval(:,i) = [vvnr{i}(4); vv{i}(4)];
    else
        alpha_est(:,i) = [NaN; NaN];
        beta_est(:,i) = [NaN; NaN];
        b(:,i) = [NaN; NaN];
       %objval(:,i) = [NaN; NaN];
    end
    
end
%%
x = 1:N;
figure(4)
subplot(4,1,1)
plot(x, alpha_est(1,:), x, alpha_est(2,:))
legend(strcat('no reset mean = ',num2str(nanmean(alpha_est(1,:)))), strcat('reset mean = ',num2str(nanmean(alpha_est(2,:)))))
subplot(4,1,2)
plot(x, beta_est(1,:), x, beta_est(2,:))
legend(strcat('no reset mean = ',num2str(nanmean(beta_est(1,:)))), strcat('reset mean = ',num2str(nanmean(beta_est(2,:)))))
subplot(4,1,3)
plot(x, b(1,:), x, b(2,:))
legend(strcat('no reset mean = ',num2str(nanmean(b(1,:)))), strcat('reset mean = ',num2str(nanmean(b(2,:)))))
subplot(4,1,4)
plot(x, objval(1,:), x, objval(2,:))
legend(strcat('no reset mean = ',num2str(nanmean(objval(1,:)))), strcat('reset mean = ',num2str(nanmean(objval(2,:)))))

%% Analyze specs
% First three in data are GLM randomly picking, no noise
% Last four are maximum is picked with noise levels of:
% 4- 0.00001
% 5- 0.0001
% 6- 0.001
% 7- 0.01