function neglogli = Loss_bernoulliGLMnd(theta, V, dV, o, T)
% neglogli = Loss_bernoulliGLM(theta, V, dV, o, T)
%
% Computes negative loglikelihood under Bernoulli GLM


alpha = theta(1);
beta = theta(2);

o = boolean(o);

lambda = exp(alpha*(V - T) + beta*dV);

nll_nospike = sum(lambda(~o));
nll_spike = -sum(log(1-exp(-lambda(o))));
neglogli = nll_nospike+nll_spike;


%m = -o*(alpha*(V - T)' + beta*dV') + sum(exp(alpha*(V - TT) + beta*dV));

%only works if all the thresh are the same 

