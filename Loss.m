function neglogli = Loss(theta, V, dV, o, T)%dV, ts, T)
alpha = theta(1);
beta = theta(2);
c = theta(3);

loglambda = alpha*(V - T) + beta*dV + c;
neglogli = -o*loglambda' + sum(exp(loglambda));


%m = -o*(alpha*(V - T)' + beta*dV') + sum(exp(alpha*(V - TT) + beta*dV));

%only works if all the thresh are the same 

