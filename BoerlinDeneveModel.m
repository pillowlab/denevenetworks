
% Set up simulation params
dt = .1/1000; % time bin size for simulations (s)
tmax = 1.5;
t = (dt:dt:tmax); % time vector
nt = length(t); % number of time pointsstim = zeros(length(t),1);

% Make stimulus
stim = zeros(1,nt);
stim(t>.25&t<.55) = 50;
stim(t>.8&t<1) = -100;

% Plot it, along with its integral
xt0 = cumsum(stim)*dt; % target (integral of stim);
plot(t,stim/10, t, xt0);

%% Initialize model
% Set up population
ncells = 400;

% decoding weight vector
ww = zeros(ncells,1);
ww(1:ncells/2) = -.1;
ww(ncells/2+1:end) = .1;
%ww = ww - 1*[abs(rand(ncells/2,1)); -1*abs(rand(ncells/2, 1))];
%ww = sort(randn(ncells,1));

% decay params
tau_d = .1; % slow decay (for rates)
tau_v = .05; % fast decay (for V)
lambda_d = 1/tau_d;
lambda_v = 1/tau_v;

% Set spike condition (random or maximal)
randomspiking=1;  % Boolean

% Costs
nu = 10^-5;
mu = 10^-6;
% mu = mu*100;
% nu = nu*100;

%% Run model

% initialize variables
Vt = zeros(ncells,nt); % voltage
ot = zeros(ncells,nt); % spikes
rt = zeros(ncells,nt); % spike rate (filtered spikes)

%xhat = zeros(1,nt);
Vtchk = zeros(ncells,nt);
xt = zeros(1,nt);

I = speye(ncells); % identity of size ncells x ncells
Thresh = .5*(mu*lambda_d^2 + nu*lambda_d + ww.^2); % spike threshold

% initialize V in first time bin
Vt(:,1) = rand(ncells,1).*Thresh*0;
Vnr = zeros(ncells,nt);
dV = zeros(ncells, nt);
%
sigr = 0;
sigv = 0.001;
%0.01 noise makes it look super irregular
%0.001 makes it look very sparse
%0.00001 makes it look like it does without randompicking

% Run simulation
for ii = 2:nt
    
    % update Vt
    dV(:,ii) = -lambda_v*dt*Vt(:,ii-1) + ww*dt*stim(ii) + ww*(ww'*lambda_d*dt*rt(:,ii-1)) + sigv*sqrt(dt)*randn(ncells,1);
    Vt(:,ii) = Vt(:,ii-1) + dV(:,ii);
    
    % update rates with decay
    rt(:,ii) = I*(1-lambda_d*dt)*rt(:,ii-1) + sigr*sqrt(dt)*randn(ncells,1);
    
    % update true x(t);
    xt(ii) = xt(ii-1)+dt*stim(ii);
    
    % Spiking
    iisp = find(Vt(:,ii)>Thresh(:));
    Vnr(:,ii) = Vt(:,ii);
    if randomspiking == 1
        if ~isempty(iisp);
            
            %random spikes
            
            if length(iisp)>1
                spikeidx = randsample(iisp,1);
            else
                spikeidx = iisp;
            end
            
            % Make spike-related updates
            ot(spikeidx,ii) = 1; % insert spike
            rt(spikeidx,ii) = rt(spikeidx,ii)+1;  % update rates
            Vt(:,ii) = Vt(:,ii) - (ww*ww'*ot(:,ii) + mu*rt(:,ii)); % membrane reset
            
            %         if max(Vt(:,ii)>.01);
            %             keyboard;
            %         end
        end
    else %randomspiking
        if ~isempty(iisp);
            spikeidx = find(Vt(:,ii) == max(Vt(iisp,ii)));
            ot(spikeidx,ii) = 1; % insert spike
            rt(spikeidx,ii) = rt(spikeidx,ii)+1;  % update rates
            Vt(:,ii) = Vt(:,ii) - (ww*ww'*ot(:,ii) + mu*rt(:,ii)); % membrane reset
            
        end
    end
    
end

%% Make plots

subplot(221);
[iiinh,jjinh] = find(ot(1:ncells/2,:));
[iiexc,jjexc] = find(ot(ncells/2+1:ncells,:));
plot(jjinh*dt,iiinh, 'b.', jjexc*dt,iiexc+ncells/2,'r.');
title('spikes'); ylabel('neuron')
set(gca, 'ylim',[0 ncells])

subplot(223);
xhat = ww'*rt;

h = plot(t,xt0,t,xhat, 'r--'); set(h(2), 'linewidth', 2);
legend('x(t)', 'xhat(t)');

subplot(222);
plot(t,rt(1:10,:), 'b', t, rt(ncells/2+[1:10],:), 'r');
xlabel('time (s)');
title('rates'); title('rates r(t)');

subplot(224);
plot(t,Vt(1:10,:), 'b-.', t, Vt(ncells/2+[1:10],:), 'r-.');
xlabel('time (s)');
ylabel('membrane potential');
title('V(t)');
%%
d = [];
d.vnr = Vnr;
d.o = ot;
d.v = Vt;
d.dv = dV;
d.T = Thresh;