muvals = [10e-7, 10e-6, 10e-5, 10e-4];
nuvals = 10*[10e-6, 10e-5, 10e-4, 10e-3];
xxh = cell(length(muvals), length(nuvals));
xx = cell(length(muvals), length(nuvals));
sps = cell(length(muvals), length(nuvals));
for ii = 1:length(nuvals)
    tic
    for jj = 1:length(muvals)
        NT =1.5; %Max time
        dt = .1/1000; %dt, arbitrary units
        time = dt:dt:NT; %Time vector
        nt = length(time);
        
        %Create stimulus
        c = zeros(1,nt);
        c(time>.25&time<.55) = 50;
        c(time>.8&time<1) = -100;
        c = c*5;
        
        % Set up population
        N = 200;
        
        % decoding weight vector
        ww = [-0.1*ones(N/2,1) + 0.05*rand(N/2,1); 0.1*ones(N/2,1) + 0.05*rand(N/2,1)];
        %ww = [-0.1*ones(N/2,1); 0.1*ones(N/2,1)];
        ww = (1/N)*100*ww;
        
        %Alpha
        alpha = 80;
        
        %Beta
        beta = -.03;
        
        %constant term
        d = 0;
        
        %Do you track w the network knowing x or not
        theoreticalx = 1;
        
        %Do we want to remove terms that scale with1;^2
        scaling = 0;
        
        % decay params
        tau_d = .1;
        lambda_d = 1/tau_d;
        lambda_s = 100;
        
        %cost params
        nu = nuvals(ii);
        mu = muvals(jj);
        % nu = nu*1/N;
        % mu = mu*1/N;
        
        % initialize variables
        o = zeros(N,nt); % spikes
        r = zeros(N,nt); % spike rate (filtered spikes)
        xh = zeros(1, nt);
        x = zeros(1,nt);
        sig = zeros(N,nt);
        
        I = speye(N); % identity of size ncells x ncells
        T = (mu*lambda_d^2 + nu*lambda_d + ww.^2)/2;
        v = zeros(N, nt);
        L = (ww.'*ww)*ww.';
        
        %Run simulation
        for i = 2:nt
            xd = -lambda_s*x(i-1) + c(i-1);
            rhd = -lambda_d*r(:,i-1) + lambda_d*o(:,i-1);
            xhd = ww.'*rhd;
            
            % update rates with decay
            r(:,i) = r(:,i-1) + rhd*dt;
            
            % update x estimate
            xh(i) = ww.'*r(:,i);
            
            if theoreticalx == 1
                % update true x(t);
                x(i) = x(i-1)+ xd*dt;
            elseif theoreticalx == 0 && scaling == 0
                % do without tracking theoretical value
                x(i) = L*v(:,i-1) + mu*lambda_d*L*r(:, i-1) + xh(i-1) + xd*dt;
            elseif theoreticalx == 0 && scaling == 1
                x(i) = mu*lambda_d*L*r(:, i-1) + xh(i-1) + xd*dt;
            end
            
            if scaling == 1
                vdot = ww*(-lambda_s*(mu*lambda_d*L*r(:, i-1) + xh(i-1))...
                    + c(i-1) - ...
                    (ww.'*(-lambda_d*r(:,i-1) + lambda_d*o(:,i-1)))) - ...
                    mu*lambda_d*(-lambda_d*r(:,i-1) + lambda_d*o(:,i-1));
            else
                vdot = ww*(-lambda_s*(L*v(:,i-1) + mu*lambda_d*L*r(:, i-1) + xh(i-1))...
                    + c(i-1) - ...
                    (ww.'*(-lambda_d*r(:,i-1) + lambda_d*o(:,i-1)))) - ...
                    mu*lambda_d*(-lambda_d*r(:,i-1) + lambda_d*o(:,i-1));
            end
            
            vdot(find(o(:, i-1) == 1)) = 0;
            v(:,i) = v(:,i-1) + vdot*dt;
            
            %Probability
            rt = exp(alpha*(v(:,i) - T) + beta*vdot + d); %Rate +log(-log(0.5)/dt));
            sig(:,i) = (1-exp(-rt*dt));
            
            % Spiking
            iisp = find(rand(N,1)<sig(:,i));
            
            if ~isempty(iisp)
                o(iisp, i) = 1;
                r(iisp, i) = r(iisp, i) + 1;
                v(:,i) = v(:,i) - (ww*ww'*o(:,i) + mu*lambda_d^2*o(:,i)); %Reset in same time bin
            end
            
        end
        
        sps{jj,ii} = o;
        xxh{jj,ii} = xh;
        xx{jj,ii} = x;
    end
    toc
end
%%

for i = 1:16
    x = xx{i};
    xh = xxh{i};
    figure(1)
    subplot(4,4,i)
    plot(time, xh, 'b-');
    hold on
    plot(time, x, 'r-')
    hold off
    
    o = sps{i};
    
    figure(2)
    subplot(4,4,i)
    [iiinh,jjinh] = find(o(1:N/2,:));
    [iiexc,jjexc] = find(o(N/2+1:N,:));
    plot(jjinh*dt,iiinh, 'b.', jjexc*dt,iiexc+N/2,'r.');
    
end