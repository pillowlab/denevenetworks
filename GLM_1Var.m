NT =1.5; %Max time
dt = .1/1000; %dt, arbitrary units
time = dt:dt:NT; %Time vector
nt = length(time);

%Create stimulus
c = zeros(1,nt);
c(time>.25&time<.55) = 50;
c(time>.8&time<1) = -100;
c = c*10;
%%
% Set up population
N = 400; 

% decoding weight vector
ww = [-0.1*ones(N/2,1) + 0.01*rand(N/2,1); 0.1*ones(N/2,1) + 0.01*rand(N/2,1)];
%ww = [-0.1*ones(N/2,1); 0.1*ones(N/2,1)];
ww = (1/N)*200*ww;

%Alpha
alpha = 80; 

%Beta
beta = -0.03;

%constant term
d = .8;

%Do you track w the network knowing x or not
theoreticalx = 1;

%Do we want to remove terms that scale with1;^2
scaling = 0; 

% decay params
tau_d = .1; 
lambda_d = 1/tau_d; 
lambda_s = 100; 

%cost params
nu = 10^-3;
mu = 10^-5;
nu = nu*1/N;
mu = mu*1/N;

% initialize variables
o = zeros(N,nt); % spikes
r = zeros(N,nt); % spike rate (filtered spikes)
xh = zeros(1, nt);
x = zeros(1,nt);
sig = zeros(N,nt);

I = speye(N); % identity of size ncells x ncells
T = (mu*lambda_d^2 + nu*lambda_d + ww.^2)/2;
v = zeros(N, nt);
L = (ww.'*ww)*ww.';

%Run simulation
for i = 2:nt
    xd = -lambda_s*x(i-1) + c(i-1);
    rhd = -lambda_d*r(:,i-1) + lambda_d*o(:,i-1);
    xhd = ww.'*rhd;
    
    % update rates with decay    
    r(:,i) = r(:,i-1) + rhd*dt;
    
    % update x estimate 
    xh(i) = ww.'*r(:,i);

    if theoreticalx == 1
    % update true x(t);
    x(i) = x(i-1)+ xd*dt;
    elseif theoreticalx == 0 && scaling == 0
    % do without tracking theoretical value
    x(i) = L*v(:,i-1) + mu*lambda_d*L*r(:, i-1) + xh(i-1) + xd*dt;
    elseif theoreticalx == 0 && scaling == 1
    x(i) = mu*lambda_d*L*r(:, i-1) + xh(i-1) + xd*dt;         
    end
    
    if scaling == 1
        vdot = ww*(-lambda_s*(mu*lambda_d*L*r(:, i-1) + xh(i-1))...
        + c(i-1) - ...
        (ww.'*(-lambda_d*r(:,i-1) + lambda_d*o(:,i-1)))) - ...
        mu*lambda_d*(-lambda_d*r(:,i-1) + lambda_d*o(:,i-1));
    else 
        vdot = ww*(-lambda_s*(L*v(:,i-1) + mu*lambda_d*L*r(:, i-1) + xh(i-1))...
        + c(i-1) - ...
        (ww.'*(-lambda_d*r(:,i-1) + lambda_d*o(:,i-1)))) - ...
        mu*lambda_d*(-lambda_d*r(:,i-1) + lambda_d*o(:,i-1));
    end
    
    vdot(find(o(:, i-1) == 1)) = 0;
    v(:,i) = v(:,i-1) + vdot*dt;
    
    %Probability
    rt = exp(alpha*(v(:,i) - T) + beta*vdot + d); %Rate +log(-log(0.5)/dt));
    sig(:,i) = (1-exp(-rt*dt));
   
    % Spiking
    iisp = find(rand(N,1)<sig(:,i));
    
    if ~isempty(iisp)
        o(iisp, i) = 1;
        r(iisp, i) = r(iisp, i) + 1;
        v(:,i) = v(:,i) - (ww*ww'*o(:,i) + mu*lambda_d^2*o(:,i)); %Reset in same time bin
    end

end
    
    

%%
%Plot response and spikes
figure(1)
clf
subplot(2,2,1)
plot(time, c)
hold off

subplot(2,2,2)
plot(time, xh, 'b-');
hold on
plot(time, x, 'r-')
hold off

subplot(2,2,3)
[iiinh,jjinh] = find(o(1:N/2,:));
[iiexc,jjexc] = find(o(N/2+1:N,:));
plot(jjinh*dt,iiinh, 'b.', jjexc*dt,iiexc+N/2,'r.');
title('spikes'); ylabel('neuron')
set(gca, 'ylim',[0 N])
hold off

subplot(2,2,4)
plot(time, sig)
%%
figure(2)
subplot(121)
re = 1/255*[255,51,51];
bl = 1/255*[0,128,255];
gr = 1/225*[247,143,39];

[iiinh,jjinh] = find(o(1:N/2,:));
[iiexc,jjexc] = find(o(N/2+1:N,:));
plot(jjinh*dt,iiinh, '.', 'Color', re)
hold on
plot(jjexc*dt,iiexc+N/2,'.', 'Color', bl);
xlabel('Time');ylabel('Neuron')
set(gca, 'ylim',[0 N])
hold on
h2 = plot(time, x*15+200, 'k-');
set(h2, 'linewidth', 2);
hold on
h1 = plot(time, xh*15+200, '-');
set(h1, 'linewidth', 2);
hold on
legend([h1, h2],'Estimate', 'Target')
legend boxoff
set(gca, 'FontSize', 15)

box off