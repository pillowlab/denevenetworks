NT =3; %Max time
dt = .1/1000; %dt, arbitrary units
time = dt:dt:NT; %Time vector
nt = length(time);

%Create stimulus
c = zeros(1,nt);
c(time>.15&time<.55) = 100; %10000 for reg two var, 100 for oscillator
%c(time>.8&time<1) = -1000;
%Two stimuli
c = [c; zeros(1, nt)];
c = c;

% Set up population
N = 400;

% decoding weight vector
%Note on random weights - works okay, find a way to formalize the
%distribution.
ww = .4*[-1*ones(N/2,1); ones(N/2,1)];
%use .1 for allspike oscillator, .4 for picking
%use .4 for either of the other one
ww = 0.1*[ww + ww.*rand(N,1), ww + ww.*randn(N,1)*2];
%ww = (1/N)*100*ww;

%Let all spike
allspike = 1; %Lets all spike


%Alpha
alpha = 130;

%beta
beta = -.03;

%constant
d = .1;

% decay params
tau_d = .1; % slow decay (for rates)
lambda_d = 1/tau_d;
lambda_s = 10*[0, -1; 1, -0.4];
%lambda_s = [-400, -800; 50, 0];

%calculate using theoretical x
theoreticalx = 1;

%cost params
nu = 10^-5;
mu = 10^-6;
nu = nu*1/N;
mu = mu*1/N;
% nu = 0;
% mu = 0;

% initialize variables
o = zeros(N,nt); % spikes
r = zeros(N,nt); % spike rate (filtered spikes)
xh = zeros(2, nt);
x = zeros(2,nt);
sig = zeros(N,nt);
v = zeros(N, nt);
theorex = zeros(2, nt);

I = speye(N); % identity of size ncells x ncells
T = (mu*lambda_d^2 + nu*lambda_d + sum(ww.^2,2))/2;
L = (ww.'*ww)*ww.';

% Run simulation
for i = 2:nt
    
    xd = lambda_s*x(:,i-1) + c(:,i-1);
    txd = lambda_s*theorex(:,i-1) + c(:, i-1);
    rhd = -lambda_d*r(:,i-1) + lambda_d*o(:,i-1);
    xhd = ww.'*rhd;
    
    % update rates with decay
    r(:,i) = r(:,i-1) + rhd*dt;
    
    % update x estimate
    xh(:,i) = ww.'*r(:,i);
    
    if theoreticalx == 1
        % update true x(t);
        x(:,i) = x(:,i-1) + xd*dt;
    else
        % do without tracking theoretical value
        x(:,i) = L*v(:,i-1) + mu*lambda_d*L*r(:, i-1) + xh(:,i-1) + xd*dt;
        theorex(:,i) = theorex(:,i-1) + txd*dt;
    end
   
    vdot = ww*(xd - xhd) - mu*lambda_d*(rhd);
    vdot(find(o(:, i-1) == 1)) = 0;
    v(:,i) = v(:,i-1) + vdot*dt;
    
    
    rt = exp(alpha*(v(:,i) - T)  + beta*vdot +d ); %Rate
    sig(:,i) = (1-exp(-rt*dt));
    
    
    % Spiking
    iisp = find(rand(N,1)<sig(:,i));
    if ~isempty(iisp)
        for k = 1:length(iisp)
        if v(iisp(k), i) > .8*T(iisp(k))
        o(iisp(k), i) = 1;
        r(iisp(k), i) = r(iisp(k), i) + 1;
        end
        end
          v(:,i) = v(:,i) - (ww*ww'*o(:,i) + mu*lambda_d^2*o(:,i)); %Reset in same time bin
    end
    
    
end
%%
%Plot response and spikes
figure(1)
subplot(2,2,1)
plot(time, c)
hold off

subplot(2,2,2)
plot(time, xh, 'b-');
hold on
if theoreticalx == 1
    plot(time, x, 'r-')
else
    plot(time, theorex, 'r-')
end
hold off

subplot(2,2,3)
[iiinh,jjinh] = find(o(1:N/2,:));
[iiexc,jjexc] = find(o(N/2+1:N,:));
plot(jjinh*dt,iiinh, 'b.', jjexc*dt,iiexc+N/2,'r.');
title('spikes'); ylabel('neuron')
set(gca, 'ylim',[0 N])
hold off

subplot(2,2,4)
plot(time, sig)
%%
figure(2)
clf
re = 1/255*[255,51,51];
bl = 1/255*[0,128,255];
gr = 1/225*[247,143,39];

h = plot(time, xh*10+200, '-', 'Color', re);
set(h, 'linewidth', 1);
hold on
h = plot(time, x*10+200, 'k-');
set(h, 'linewidth', 1);
hold on

[iiinh,jjinh] = find(o(1:N/2,:));
[iiexc,jjexc] = find(o(N/2+1:N,:));
plot(jjinh*dt,iiinh, '.', 'Color', re)
hold on
plot(jjexc*dt,iiexc+N/2,'.', 'Color', bl);
xlabel('Time');ylabel('Neuron')
set(gca, 'ylim',[0 N])
hold on

set(gca, 'FontSize', 12)

box off