clf

NT =50; %Max time
dt = .1; %dt, arbitrary units
time = dt:dt:NT; %Time vector
T = 0.2; % threshold
nt = length(time); % number of time bins

tau = 20;
alpha = 100;
tauc = 10; %Decay of stimulus
beta = .3;

lambda = 0.0500;

reps = 20; % number of repeats of GLM simulation
rasterplot = zeros(reps+1, nt);

c = zeros(1,nt);
lambda_c = 1/(2*tauc); %Decay

% --- Make OU stimulus -----
for i = 2:nt
    dc = -lambda_c*c(i-1) + 0.1*randn();
    c(i) = c(i-1) + dc;
end

% % --- Make smoothed Gaussian stimulus----
% c = gsmooth(randn(nt,1),10);
% lambda = 1/(tau); %Decay


lif_v = zeros(1,nt); %Voltages
lif_sp = zeros(1,nt); %Spike times
lif_vnr = zeros(1,nt);
dv = zeros(1,nt);

subplot(221);
plot(c);
title('stimulus'); axis tight;
xlabel('time (bins)');

%% Simulate LIF neuron
for i = 2:nt
    if lif_sp(i-1)>0
        dv(i) = 0;
    else
        dv(i) = -lambda*lif_v(i-1) + c(i);
    end
    lif_v(i) = lif_v(i-1) + dv(i)*dt;
    lif_vnr(i) = lif_v(i);
    if lif_v(i) > T
        lif_sp(i) = 1; %Spike event
        lif_v(i) = -2*T; %Reset to -2*Threshold
    end
end
tsp = dt*find(lif_sp); % spike times
rasterplot(reps+1,:) = lif_sp;

subplot(222);
plot(time,lif_vnr, time, dv, 'k',tsp, T, 'rx');

title('V');

%% Now do GLM fitting for all params

% Fit GLM
options = optimset('Display', 'iter', 'largescale', 'off');
fun = @(x) Loss_bernoulliGLM(x, lif_vnr, dv, lif_sp, T);
x0 = [0;0;0];
[theta] = fminunc(fun,x0, options);

theta

% Extract ML param estimates
alpha_hat = theta(1);
beta_hat = theta(2);
d_hat = theta(3);

subplot(426);
lambda_pred = (alpha_hat*(lif_vnr-T)+beta_hat*dv+d_hat);
plot(time, lif_vnr, tsp, 1, 'rx')
hold on
%% Simulate GLM with fitted parameters
reps = 20;
Mtsp = cell(1,reps);
allsp = zeros(reps, nt);

for j = 1:reps
    Mtsp{j} = [];
    n_glm_v = zeros(1,nt);
    n_glm_sp = zeros(1,nt);
    n_glm_vnr = zeros(1,nt);
    lam = zeros(1,nt);
    for i = 2:nt
        
        if n_glm_sp(i-1)>0
            gldv = 0;
        else
            gldv = -lambda*n_glm_v(i-1) + c(i);
        end
        
        gldv = -lambda*n_glm_v(i-1) + c(i);
        n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
        n_glm_vnr(i) = n_glm_v(i);
        rt(i) = exp(alpha_hat*(n_glm_v(i)-T)+ beta_hat*gldv+d_hat); %Rate
        Ps(i) = (1-exp(-rt(i)*dt));
        lam(i) = alpha_hat*(n_glm_v(i)-T) + beta_hat*gldv+d_hat;
        if rand() < Ps(i) %Condition for spiking
            n_glm_sp(i) = 1; %Spike
            n_glm_v(i) = -2*T; %Reset
        end
        
    end
    Mtsp{j} = find(n_glm_sp)*dt;
    allsp(j,:) = n_glm_sp;
end
tspnglm =find(n_glm_sp)*dt;
plot(time, n_glm_v, tspnglm, 1, 'b.')
hold off

subplot(223);
plotRaster(Mtsp);
set(gca,'xlim', [0 nt*dt
    ]);
title('GLM repeats');
hold off


subplot(428);
logrtplot = log(max(rt,1e-200));
plot(time, lam)
hold on
plot(time, lambda_pred)
hold off

%% Make figs

re = 1/255*[255,51,51];
bl = 1/255*[0,128,255];
gr = 1/225*[22, 183, 32];

figure(2)
clf
plot(time, c, 'Color', bl)
title 'Stimulus'
xlabel 'Time'
grid on

box off
set(gca, 'FontSize', 15)
saveas(gca, 'OUStim', 'png')

figure(3)
clf
plot(time, lif_vnr, 'Color', re)
hold on
plot(time, n_glm_vnr, 'Color', bl)
legend('LIF neuron', 'GLM neuron')
hold on
plot(tsp, T,'x', 'Color', re)
hold on
plot(tspnglm, T, 'b.')
hold off
grid on
title 'Voltage'
xlabel 'Time'

box off
set(gca, 'FontSize', 15)
saveas(gca, 'SingleNeuronFit', 'png')
%%
figure(1)
clf

plot(time, lam)
hold on
plot(time, lambda_pred)
legend('\lambda', '\lambda predicted')
plot(tsp, T,'x', tspnglm, T,'x')
legend(gca, 'boxoff')
hold off
box off
grid on
xlabel Time
ylabel Intensity
set(gca, 'FontSize', 12)

%% Get PSTH
sum = zeros(1,nt);
for i = 1:reps
    sum = sum + allsp(i,:);
end
sum = sum./reps;
figure(4)
plot(time, sum)

%% =================================
% Now fix alpha and just fit beta and d

alphavals = [100, 250, 500, 1000, 2000];
allsp = cell(1,length(alphavals));

for l = 1:length(alphavals)
    alpha_fix = alphavals(l);
    fun2 = @(prs)Loss_bernoulliGLM([alpha_fix; prs], lif_vnr, dv, lif_sp, T);
    
    prs0 = [0;0];
    
    theta2 = fminunc(fun2,prs0, options);
    
    theta2
    beta_hat2 = theta2(1);
    d_hat2 = theta2(2);
    
    reps = 10000;
    allsp{l} = zeros(j, nt);
    
    for j = 1:reps
        n_glm_v = zeros(1,nt);
        n_glm_sp = zeros(1,nt);
        n_glm_vnr = zeros(1,nt);
        for i = 2:nt
            
            if n_glm_sp(i-1)>0
                gldv = 0;
            else
                gldv = -lambda*n_glm_v(i-1) + c(i);
            end
            
            gldv = -lambda*n_glm_v(i-1) + c(i);
            n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
            n_glm_vnr(i) = n_glm_v(i);
            rt(i) = exp(alpha_fix*(n_glm_v(i)-T)+ beta_hat2*gldv+d_hat2); %Rate
            Ps(i) = (1-exp(-rt(i)*dt));
            lam(i) = alpha_fix*(n_glm_v(i)-T) + beta_hat2*gldv+d_hat2;
            if rand() < Ps(i) %Condition for spiking
                n_glm_sp(i) = 1; %Spike
                n_glm_v(i) = -2*T; %Reset
            end
            
        end
        allsp{l}(j,:) = n_glm_sp;
    end
    tspnglm =find(n_glm_sp)*dt;
    l
    
end
%%
figure(4)
clf
avsp = cell(1, length(alphavals));
for j = 1:length(alphavals)
    sum = zeros(1,nt);
    for i = 1:reps
        sum = sum + allsp{j}(i,:);
    end
    sum = sum./reps;
    avsp{j} = sum;
    plot(time, sum)
    hold on
end
hold off
legend(num2str(betavals(1)), num2str(betavals(2)), num2str(betavals(3)), num2str(betavals(4)), num2str(betavals(5)))
legend(gca, 'boxoff')
%% =================================
% Now fix beta and fit alpha and d

betavals = -10:10;
allsp = cell(1,length(betavals));

for l = 1:length(betavals)
    beta_fix = betavals(l);
    fun2 = @(prs)Loss_bernoulliGLM([prs(1), beta_fix, prs(2)], lif_vnr, dv, lif_sp, T);
    
    prs0 = [0;0];
    
    theta2 = fminunc(fun2,prs0, options);
    
    theta2
    alpha_hat2 = theta2(1);
    d_hat2 = theta2(2);
    
    reps = 10000;
    allsp{l} = zeros(j, nt);
    
    for j = 1:reps
        n_glm_v = zeros(1,nt);
        n_glm_sp = zeros(1,nt);
        n_glm_vnr = zeros(1,nt);
        for i = 2:nt
            
            if n_glm_sp(i-1)>0
                gldv = 0;
            else
                gldv = -lambda*glm_v(i-1) + c(i);
            end
            
            gldv = -lambda*n_glm_v(i-1) + c(i);
            n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
            n_glm_vnr(i) = n_glm_v(i);
            rt(i) = exp(alpha_hat2*(n_glm_v(i)-T)+ beta_fix*gldv+d_hat2); %Rate
            Ps(i) = (1-exp(-rt(i)*dt));
            lam(i) = alpha_hat2*(n_glm_v(i)-T) + beta_fix*gldv+d_hat2;
            if rand() < Ps(i) %Condition for spiking
                n_glm_sp(i) = 1; %Spike
                n_glm_v(i) = -2*T; %Reset
            end
            
        end
        allsp{l}(j,:) = n_glm_sp;
    end
    tspnglm =find(n_glm_sp)*dt;
    l
    
end
%%
figure(5)
clf
avsp = cell(1, length(betavals));
for j = 1:length(betavals)
    sum = zeros(1,nt);
    for i = 1:reps
        sum = sum + allsp{j}(i,:);
    end
    sum = sum./reps;
    avsp{j} = sum;
    plot(time, sum)
    hold on
    pause(1)
end
plot(tsp, 1,'rx')
hold off
%legend(num2str(betavals(1)), num2str(betavals(2)), num2str(betavals(3)), num2str(betavals(4)), num2str(betavals(5)))
%legend(gca, 'boxoff')

%% =================================
% Vary alpha, fitted beta and d

alphavals = 50:50:round(alpha_hat, -2);
allsp = cell(1,length(alphavals));

for l = 1:length(alphavals)
    alpha = alphavals(l);
    reps = 10000;
    allsp{l} = zeros(j, nt);
    
    for j = 1:reps
        n_glm_v = zeros(1,nt);
        n_glm_sp = zeros(1,nt);
        n_glm_vnr = zeros(1,nt);
        for i = 2:nt
            
            if n_glm_sp(i-1)>0
                gldv = 0;
            else
                gldv = -lambda*glm_v(i-1) + c(i);
            end
            
            gldv = -lambda*n_glm_v(i-1) + c(i);
            n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
            n_glm_vnr(i) = n_glm_v(i);
            rt(i) = exp(alpha*(n_glm_v(i)-T)+ beta_hat*gldv+d_hat); %Rate
            Ps(i) = (1-exp(-rt(i)*dt));
            
            if rand() < Ps(i) %Condition for spiking
                n_glm_sp(i) = 1; %Spike
                n_glm_v(i) = -2*T; %Reset
            end
            
        end
        allsp{l}(j,:) = n_glm_sp;
    end
    tspnglm =find(n_glm_sp)*dt;
    
    
end
%%
figure(6)
clf
avsp = cell(1, length(alphavals));
for j = 1:length(alphavals)
    sum = zeros(1,nt);
    for i = 1:reps
        sum = sum + allsp{j}(i,:);
    end
    sum = sum./reps;
    avsp{j} = sum;
    plot(time, sum)
    hold on
    pause(1)
end
plot(tsp, 1,'rx')
hold off
%legend(num2str(betavals(1)), num2str(betavals(2)), num2str(betavals(3)), num2str(betavals(4)), num2str(betavals(5)))
%legend(gca, 'boxoff')

%% =================================
% Vary alpha, no beta or d

alphavals = 0:5:round(alpha_hat,-2)/60;
allsp = cell(1,length(alphavals));

for l = 1:length(alphavals)
    alpha = alphavals(l);
    reps = 10000;
    allsp{l} = zeros(j, nt);
    
    for j = 1:reps
        n_glm_v = zeros(1,nt);
        n_glm_sp = zeros(1,nt);
        n_glm_vnr = zeros(1,nt);
        for i = 2:nt
            
            if n_glm_sp(i-1)>0
                gldv = 0;
            else
                gldv = -lambda*n_glm_v(i-1) + c(i);
            end
            
            gldv = -lambda*n_glm_v(i-1) + c(i);
            n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
            n_glm_vnr(i) = n_glm_v(i);
            rt(i) = exp(alpha*(n_glm_v(i)-T)); %Rate
            Ps(i) = (1-exp(-rt(i)*dt));
            
            if rand() < Ps(i) %Condition for spiking
                n_glm_sp(i) = 1; %Spike
                n_glm_v(i) = -2*T; %Reset
            end
            
        end
        allsp{l}(j,:) = n_glm_sp;
    end
    tspnglm =find(n_glm_sp)*dt;
    
    
end
%%
figure(7)
subplot(2,1,2)
avsp = cell(1, length(alphavals));
for j = 2:2:length(alphavals)-4
    sum = zeros(1,nt);
    for i = 1:reps
        sum = sum + allsp{j}(i,:);
    end
    sum = sum./reps;
    avsp{j} = sum;
    plot(time, sum*3.2)
    hold on
    pause(1)
end
plot(tsp, 1,'rx')
hold off
grid on
box off
xlabel 'Time'
ylabel 'Probability of spike'
legend('.1\alpha', '.2\alpha', '.4\alpha')
legend(gca, 'boxoff')
set(gca, 'FontSize', 15)
saveas(gca, 'MSPT', 'png')
%legend(num2str(betavals(1)), num2str(betavals(2)), num2str(betavals(3)), num2str(betavals(4)), num2str(betavals(5)))
%legend(gca, 'boxoff')

%% =================================
% Fixed fitted alpha, vary beta

betavals = -2*round(beta_hat):round(beta_hat):2*round(beta_hat);
allsp = cell(1,length(betavals));
alpha = alpha_hat;
for l = 1:length(betavals)
    beta = betavals(l);
    
    reps = 1000;
    allsp{l} = zeros(j, nt);
    
    for j = 1:reps
        n_glm_v = zeros(1,nt);
        n_glm_sp = zeros(1,nt);
        n_glm_vnr = zeros(1,nt);
        for i = 2:nt
            
            if n_glm_sp(i-1)>0
                gldv = 0;
            else
                gldv = -lambda*n_glm_v(i-1) + c(i);
            end
            
            gldv = -lambda*n_glm_v(i-1) + c(i);
            n_glm_v(i) = n_glm_v(i-1) + gldv*dt;
            n_glm_vnr(i) = n_glm_v(i);
            rt(i) = exp(alpha*(n_glm_v(i)-T) + beta*gldv); %Rate
            Ps(i) = (1-exp(-rt(i)*dt));
            
            if rand() < Ps(i) %Condition for spiking
                n_glm_sp(i) = 1; %Spike
                n_glm_v(i) = -2*T; %Reset
            end
            
        end
        allsp{l}(j,:) = n_glm_sp;
    end
    tspnglm =find(n_glm_sp)*dt;
    
    
end
%%
figure(8)
clf
avsp = cell(1, length(betavals));
for j = 1:length(betavals)
    sum = zeros(1,nt);
    for i = 1:reps
        sum = sum + allsp{j}(i,:);
    end
    sum = sum./reps;
    avsp{j} = sum;
    plot(time, sum*1/0.75)
    hold on
    pause(1)
    j
end
plot(tsp, 1,'rx')
hold off
box off
grid on
legend('-\beta', '0', 'beta')
legend(gca, 'boxoff')
xlabel 'Time'
ylabel 'Probability of spiking'


%%
figure(9)
t1 = zeros(1,length(betavals));
t2 = zeros(1,length(betavals));
t3 = zeros(1,length(betavals));
t4 = zeros(1, length(betavals));
t5 = zeros(1, length(betavals));
t6 = zeros(1, length(betavals));
for j = 1:length(betavals)
    sum = avsp{j};
    search = sum(150:160);
    'here'
    aa = time(find(search == max(sum(150:160)))+150)
    if(length(aa) > 1)
        t1(j) = aa(1);
    else
        t1(j) = aa(1);
    end
    
    search = sum(180:216);
    bb = time(find(search == max(sum(180:216)))+180)
    if(length(bb) > 1)
        t2(j) = bb(2)
    else
        t2(j) = bb
    end
    
    search = sum(218:230);
    cc = time(find(search == max(sum(218:230)))+218)
    if(length(cc) > 1)
        t3(j) = cc(end)
    else
        t3(j) = cc
    end
    
    search = sum(310:320);
    dd = time(find(search == max(sum(310:320)))+310)
    if(length(dd) > 1)
        mn = dd - tsp(4);
        val = find(mn == min(mn));
        t4(j) = dd(val);
    else
        t4(j) = dd
    end
%     
%     search = sum(400:456);
%     ee = time(find(search == max(sum(400:456)))+400)
%     if(length(ee) > 1)
%         t5(j) = ee(end)
%     else
%         t5(j) = ee
%     end
%     
%     search = sum(458:end);
%    ff = time(find(search == max(sum(458:end)))+458)
%     if(length(ff) > 1)
%         t6(j) = ff(end)
%     else
%         t6(j) = ff
%     end
end
%%
figure(9)
clf
plot(tsp,0, 'kx')
hold on
aa = -(t1(1:2:end) - tsp(1)) + tsp(1)
plot(aa, (-abs(betavals(1:2:end)/round(beta_hat))+1)/2)
bb = -(t2(1:2:end) - tsp(2)) + tsp(2)
plot(bb, (-abs(betavals(1:2:end)/round(beta_hat))+1)/2)
cc = -(t3(1:2:end) - tsp(3)) + tsp(3)
plot(cc, (-abs(betavals(1:2:end)/round(beta_hat))+1)/2)

dd = -(t4(1:2:end) - tsp(4)) + tsp(4)
plot(dd, (-abs(betavals(1:2:end)/round(beta_hat))+1)/2)

ee = -(t5(1:2:end) - tsp(5)) + tsp(5)
plot(ee, (-abs(betavals(1:2:end)/round(beta_hat))+1)/2)
grid on
box off
xlabel 'Time'
ylabel '\beta '
set(gca, 'FontSize', 12)
saveas(gca, 'betaval', 'png')
%%
for i = 1:2:length(t1)
    
    t1p(i) = t1(i);
    t2p(i) = t2(i);
    t3p(i) = t3(i);
    t4p(i) = t4(i);
    t5p(i) = t5(i);
end

%%

figure(10)
clf
plot(tsp,1, 'x')
hold on
aa = -(t1p - tsp(1)) + tsp(1)
plot(aa, -abs(betavals/round(beta_hat))+1)
bb = -(t2p - tsp(2)) + tsp(2)
plot(bb, -abs(betavals/round(beta_hat))+1)
cc = -(t3p - tsp(3)) + tsp(3)
plot(cc, -abs(betavals/round(beta_hat))+1)

dd = -(t4p - tsp(4)) + tsp(4)
plot(dd, -abs(betavals/round(beta_hat))+1)

ee = -(t5p - tsp(5)) + tsp(5)
plot(ee, -abs(betavals/round(beta_hat))+1)
%%

t2(5) = t2(5) + 2
t2(4) = t2(4) + 2

%%
clf
plot(t1 - tsp(1)-.1, betavals./beta_hat+1, 'x-', 'LineWidth', 1)
pause(1)
hold on
plot(t2 - tsp(2), betavals./beta_hat+1, 'x-', 'LineWidth', 1)
pause(1)
hold on
plot(t3 - tsp(3) + 0.2, betavals./beta_hat+1, 'x-', 'LineWidth', 1)
pause(1)
hold on
plot(t4 - tsp(4), betavals./beta_hat+1, 'x-', 'LineWidth', 1)
pause(1)
%yticklabels({'-6\beta', '-4\beta','-2\beta','\beta = 0', '2\beta', '4\beta', '6\beta'})
%xticklabels({-1 -0.8 -0.6 -0.4 -0.2 'Time of Spike' 0.2 0.4})
grid on
box off
% xlim([-0.81  0.41])
% ylim([-4.3 6.3])
plot([-1 0], [1 1], '- r', 'LineWidth', .5)
plot([0 0], [-1 1], '- r', 'LineWidth', .5)
xlabel('Time')
ylabel('\beta value')
set(gca, 'FontSize', 15)
saveas(gca, 'Betatime', 'png')